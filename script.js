document.addEventListener("DOMContentLoaded", function() {
    const showPasswordIcons = document.querySelectorAll(".icon-password");

    showPasswordIcons.forEach(icon => {
        icon.addEventListener("click", () => {
            const input = icon.previousElementSibling;
            const isPasswordFieldVisible = input.type === "text";

            if (isPasswordFieldVisible) {
                input.type = "password";
                icon.classList.remove("fa-eye-slash");
                icon.nextElementSibling.style.display = "none";
            } else {
                input.type = "text";
                icon.classList.add("fa-eye-slash");
                icon.nextElementSibling.style.display = "inline-block";
            }
        });
    });

    const submitButton = document.getElementById("submitButton");
    const errorMessage = document.getElementById("errorMessage");

    submitButton.addEventListener("click", function() {
        const passwordInput = document.getElementById("passwordInput");
        const confirmPasswordInput = document.getElementById("confirmPasswordInput");

        const passwordValue = passwordInput.value;
        const confirmPasswordValue = confirmPasswordInput.value;

        if (passwordValue === confirmPasswordValue) {
            alert("You are welcome");
            errorMessage.textContent = "";
        } else {
            errorMessage.textContent = "Потрібно ввести однакові значення";
        }
    });
    const themeButton = document.getElementById("themeButton");
    let currentTheme = "theme1"; // Initial theme

    themeButton.addEventListener("click", function() {
        // Toggle themes
        if (currentTheme === "theme1") {
            document.body.classList.remove("theme1");
            document.body.classList.add("theme2");
            currentTheme = "theme2";
        } else {
            document.body.classList.remove("theme2");
            document.body.classList.add("theme1");
            currentTheme = "theme1";
        }

        // Store the current theme in local storage
        localStorage.setItem("currentTheme", currentTheme);
    });

    // Check for the stored theme on page load
    const storedTheme = localStorage.getItem("currentTheme");
    if (storedTheme) {
        document.body.classList.add(storedTheme);
        currentTheme = storedTheme;
    }
});

